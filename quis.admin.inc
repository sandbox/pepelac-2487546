<?php
/**
 *  @file
 *  Обратные вызовы админки модуля носпам
 */
function quis_admin_settings() {
	drupal_add_css(drupal_get_path('module','quis').'/quis.css');
	drupal_add_js(drupal_get_path('module','quis').'/quis.js');
	$options = node_get_types('names');
	$quis_count_rule = variable_get('quis_count_rule', array());
	$quis_rule = variable_get('quis_rule', array());
	$quis_words = variable_get('quis_words', array('фильмы онлайн,порно-сайт'));
	$quis_spamers_actions = variable_get('quis_spamers_actions', array(array('log')));
	$quis_rules_messages = variable_get('quis_rules_messages', array());
	$quis_materials_actions = variable_get('quis_materials_actions', array('unaction'));
	$mb_convert_case = function_exists('mb_convert_case');
	$form['quis_active'] = array(
		'#type' => 'checkbox',
		'#title' => 'Включить работу модуля',
		'#attributes' => array('class' => 'quis-enabled'),
		'#description' => 'При включеном состоянии модуль будет проверять отмеченные типы материалов на предмет содержания спама.',
		'#default_value' => variable_get('quis_active', 0),
	);
	if(!$mb_convert_case) {
		$form['quis_active']['#attributes']['disabled'] = 'disabled';
		$form['quis_active']['#description'] = 'Для работы, в начале, необходимо включить поддержку php-функции - `mb_convert_case()`';
	}
	$types_nodes = variable_get('quis_node_types', array());
	$count=0;
	foreach ($types_nodes AS $value) {
		if($value) {
			$count++;
		}
	}
	$form['type_block'] = array(
		'#type' => 'fieldset',
		'#title' => 'Типы материалов для проверки (Указано '.$count.' из '.sizeof($types_nodes).')',
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);	
	$form['type_block']['quis_node_types'] = array(
		'#type' => 'checkboxes',
		'#options' => $options,
		'#default_value' => $types_nodes,
		'#description' => 'В выбранных типах будет осуществляться поиск спама',
		
	);
	$form['rule[0]'] = array(
		'#type' => 'fieldset',
		'#title' => 'Правило #1 ',
		'#collapsible' => TRUE,
		'#attributes' => array('class' => (($quis_rule[0]) ? '': 'quis-rule-unactive')),
		'#collapsed' => TRUE,
	);
	$form['rule[0]']['quis_count_rule[0]'] = array(
		'#type' => 'hidden',
		'#value' => 1,
	);
	$form['rule[0]']['quis_rule[0]'] = array(
		'#type' => 'checkbox',
		'#title' => 'Активировать это правило',
		'#default_value' => $quis_rule[0],
	);
	$form['rule[0]']['quis_words[0]'] = array(
		'#type' => 'textarea',
		'#default_value' => mb_convert_case($quis_words[0], MB_CASE_LOWER, "UTF-8"),
		'#title' => 'Нехорошие слова и фразы',
		'#description' => 'Введите слова чередуя запятой (регистр букв необязателен).',
	);
	$form['rule[0]']['quis_spamers_actions[0]'] = array(
		'#type' => 'checkboxes',
		'#options' => quis_options_spamers(),
		'#title' => 'Операции с выявленным спамером',
		'#description' => 'Автоматическое действие c выявленным спаммером',
		'#default_value' => $quis_spamers_actions[0],
	);
	$form['rule[0]']['quis_rules_messages[0]'] = array(
		'#type' => 'textfield',
		'#title' => 'Предупреждающее сообщение',
		'#default_value' => $quis_rules_messages[0],
	);
	$form['rule[0]']['quis_materials_actions[0]'] = array(
		'#type' => 'radios',
		'#options' => quis_options_material(),
		'#title' => 'Операции с выявленным материалом',
		'#description' => 'Автоматическое действие c выявленным спам-материалом',
		'#default_value' => $quis_materials_actions[0],
	);
	
	for($i = 1; $i < sizeof($quis_count_rule); $i++) {
		$form[] = quis_get_rule_block($i, $quis_rule[$i], $quis_words[$i], (array)$quis_spamers_actions[$i], $quis_materials_actions[$i], $quis_rules_messages[$i]);
	}
	
	$form['add_rule'] = array(
		'#prefix' => '<div id="wrapper-new-block">',
		'#suffix' => '</div><br/>',
		'#type' => 'button',
		'#attributes' => array('onClick' => 'return false;','style' => 'float:right'),
		'#value' => '+ новое правило',
	);
	
	$form['quis_cicle_break'] = array(
		'#type' => 'checkbox',
		'#title' => 'Стоп цикл правил (увеличение производительности) (внимание!)',
		'#default_value' => variable_get('quis_cicle_break', 1),
		'#description' => 'Если включено, то при срабатывании одного правила, последующие - не будут вызываться. Таким образом, более агрессивные правила должны использоваться приоритетным образом. Более щадящие - в последнюю очередь. Иначе, при создании материала содержащего запись: `кино проститутки онлайн скачать реферат` и при трех правилах: 1 - `кино`, 2 - `проститутки онлайн`, 3 - `скачать реферат`, где  если 3-е правило будет самым агрессивным, то при включенной опции до него в цикле просто не дойдет очередь.',
	);
	
	$form['quis_comments_active'] = array(
		'#type' => 'checkbox',
		'#title' => 'Проверка комментариев',
		'#default_value' => variable_get('quis_comments_active',0),
		'#description' => 'Если отмечено, все входящие комментарии будут анализироваться на спам-принадлежность независимо от типа материала.',
	);
	$form['quis_delete_log'] = array(
		'#type' => 'checkbox',
		'#title' => 'Удалять лог',
		'#description' => 'Если вы не хотите чтобы лог-запись оставалась после удаления материала активируйте эту опцию.',
		'#default_value' => variable_get('quis_delete_log', 0),
	);
	
	$form['spam_block'] = array(
		'#type' => 'fieldset',
		'#title' => 'Отображение слов столбиками для удобного просмотра (В алфавитном порядке)',
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);
	$text_array_spamblock = quis_get_words_arrays_merge($quis_words);
	natsort($text_array_spamblock);
	$form['spam_block']['a']	= array(
		'#type' => 'item',
		'#prefix' => '<div class="quis-columns-text">',
		'#value' => implode("<br/>",$text_array_spamblock),
		'#suffix' => '</div>', 
	);
	$form['quis_send_mail']=array(
		'#type' => 'textfield',
		'#title' => 'Оповещение',
		'#description' => 'Почтовый ящик, на который будет приходить уведомление о найденных спам-сообщениях в каждое прохождение cron.',
		'#default_value' => variable_get('quis_send_mail',''),
	);
	$is_debug = variable_get('quis_debug', 0);
	$form['quis_debug'] = array(
		'#type' => 'checkbox',
		'#title' => 'Включить отладку',
		'#default_value' => $is_debug,
		'#description' => 'После сохранения будет произведен поиск по вхождениям указанных слов в объемном тексте.',
		'#suffix' => (($is_debug) ? '<div>Поиск выполнен за '.quis_debug().' sec.</div>' : ''),
		'#access' => FALSE,
	);
	$form['#submit'][] = 'util_admin_save';
	
	
	return system_settings_form($form);
}
/**
 * callback save function
 * admin panel
 */
function util_admin_save(&$form, &$form_state) {
	if(is_array($form_state['clicked_button']['#post']['quis_rule_delete'])) {
		foreach ($form_state['clicked_button']['#post']['quis_rule_delete'] AS $key => $value) {
			if($value) {
				unset($form_state['clicked_button']['#post']['quis_count_rule'][$key]);
				unset($form_state['clicked_button']['#post']['quis_rule'][$key]);
				unset($form_state['clicked_button']['#post']['quis_words'][$key]);
				unset($form_state['clicked_button']['#post']['quis_spamers_actions'][$key]);
				unset($form_state['clicked_button']['#post']['quis_materials_actions'][$key]);
				unset($form_state['clicked_button']['#post']['quis_rules_messages'][$key]);
			}
		}
	}
	variable_set('quis_count_rule', $form_state['clicked_button']['#post']['quis_count_rule']);
	variable_set('quis_rule', $form_state['clicked_button']['#post']['quis_rule']);
	variable_set('quis_words', $form_state['clicked_button']['#post']['quis_words']);
	variable_set('quis_spamers_actions', $form_state['clicked_button']['#post']['quis_spamers_actions']);
	variable_set('quis_materials_actions', $form_state['clicked_button']['#post']['quis_materials_actions']);
	variable_set('quis_rules_messages', $form_state['clicked_button']['#post']['quis_rules_messages']);
}
/**
 * Отображение списка спам-материалов
 *
 */
function quis_spamers_list() {
	global $base_url;
	if(isset($_POST['for_delete'])) {
		$ok = db_delete('quis_log')
		->condition('id', $_POST['for_delete'], 'IN')
		->execute();
		if($ok){
			drupal_set_message('Данные были успешно удалены!');
		}
	}
	drupal_add_css(drupal_get_path('module','quis').'/quis.css');
	drupal_add_js(drupal_get_path('module','quis').'/quis.js');
/*	$query = "SELECT DISTINCT 
											l.id
										,	u.name
										, l.spam	
										, l.nid log_nid
										, l.uid log_uid
										, l.needle
										, u.mail
										, u.status user_status
										, n.status node_status
										, n.nid node_nid
										, n.title
										, l.created log_created
			FROM quis_log l
				LEFT JOIN {users} u ON l.uid = u.uid
				LEFT JOIN {node} n ON l.nid = n.nid && l.uid = n.uid
			WHERE l.cid = 0	
	";*/
	
	$result = db_select("quis_log", "ql");
	$result->distinct();
	$result->leftJoin('users','u', 'l.uid = u.uid');
	$result->leftJoin('node', 'n', 'l.nid = n.nid && l.uid = n.uid');
	$result->fields('l', array(
		'id',
		'spam',
		'nid' => 'log_nid',
		'uid' => 'log_uid',
		'needle',
		'created' => 'log_created'
	))->fields('u', array(
		'name',
		'mail',
		'status' => 'user_status',
	))->fields('n', array(
		'status' => 'node_status',
		'nid' => 'node_nid',
		'title'
	))
	->condition('l.cid', 0)
	->execute()->fetchAll();
	
	
	$header = array(
		array('data' => 'Записано', 'field' => 'log_created','sort' => 'desc'),
		array('data' => 'Пользователь', 'field' => 'name'),
		array('data' => 'mail', 'field' => 'mail'),
		array('data' => 'Узел', 'field' => 'title'),
		array('data' => 'Уч. запись', 'field' => 'user_status'),
		array('data' => 'Материал', 'field' => 'node_status'),
		array('data' => 'Спам'),
		array('data' => 'Найдено'),
		array('data' => '<input type="checkbox" name="quis_all_check">'),
	);
	$query .= tablesort_sql($header);
	$result = pager_query($query, 25, 0, NULL);
	$c = 0;
	while($row = db_fetch_object($result)) {
		$rows[$c] = array(
			array('data' => format_date($row->log_created,'custom','d M Y, H:i:s')),
			array('data' => ($row->name) ? l($row->name, 'user/'.$row->log_uid) . ' '.$row->log_uid : '...'),
			array('data' => ($row->mail) ? $row->mail : '...'),
		);
		
		if($row->node_nid) {
			$rows[$c][]['data'] = l($row->title, 'node/'.$row->node_nid) . ' '. $row->log_nid;
		}
		else {
			$rows[$c][]['data'] = '...';
		}
		
		if($row->name) {
			$rows[$c][]['data'] =  ($row->user_status) ? '<span class="quis_green">активна</span>' :'<span class="quis_orange">блокирована</span>';
		}
		else {
			$rows[$c][]['data'] =  '<span class="quis_gray">удалена</span>';
		}
		
		if($row->node_nid) {
			$rows[$c][]['data'] =  ($row->node_status) ? '<span class="quis_green">опубликован</span>' :'<span class="quis_orange">неопубликован</span>';
		}
		else {
			$rows[$c][]['data'] =  '<span class="quis_gray">Удален</span>';
		}
		
		if($row->spam != "") {
			$rows[$c][]['data'] =  '<a href="#openModal-'.$row->log_nid.'" class="openModal" path="'.$GLOBALS['base_url'].'" nid="'.$row->log_nid.'" type="material">Просмотр</a>'.quis_textmodal($row->log_nid);
		}
		else {
			$rows[$c][]['data'] =  '..';
		}
		
		if($row->needle != "") {
		  $rows[$c][]['data'] = $row->needle;
		}
		else {
		  $rows[$c][]['data'] = '..';
		}
		
		$rows[$c][]['data']='<input type="checkbox" name="for_delete['.$row->id.']" value="1">';
		$c++;
	}
	global $base_url;
	$output .= '<form action="'.$base_url.'/admin/settings/quis/log"  accept-charset="UTF-8" method="post" id="quis-admin-settings-comments">';
	$output .= theme('pager', NULL, 25, 0);
	$output .= theme('table', $header, $rows);
	$output .= theme('pager', NULL, 25, 0);
	$output .= '<div class="quis-button"><input type="submit" value="Удалить отмеченные" onClick="return confirm(\'Уверены?\')"></div>';
	$output .= '</form>';
	return $output;
}
/**
 * Отображение списка спам-комментариев
 *
 */
/*function quis_spamers_comments_list() {
	global $base_url;
	if(isset($_POST['for_delete'])) {
		if(db_query("DELETE FROM {quis_log} WHERE id IN(%s)", implode(",",array_keys($_POST['for_delete'])))){
			drupal_set_message('Данные были успешно уудалены');
		}
	}
	drupal_add_css(drupal_get_path('module','quis').'/quis.css');
	drupal_add_js(drupal_get_path('module','quis').'/quis.js');
	$query = "SELECT 
											u.name
										, l.spam
										, l.id	
										, l.nid log_nid
										, l.uid
										, u.mail
										, u.status user_status
										, l.created log_created
										, c.subject
										, c.cid comment_cid
										, l.cid log_cid
										, c.nid comment_nid
										, c.status comment_status
			FROM quis_log l
				LEFT JOIN {comments} c ON l.cid = c.cid && l.uid = c.uid
				LEFT JOIN {users} u ON l.uid = u.uid
			WHERE l.cid != 0	
	";
	$header = array(
		array('data' => 'записано', 'field' => 'log_created','sort' => 'desc'),
		array('data' => 'пользователь', 'field' => 'name'),
		array('data' => 'mail', 'field' => 'mail'),
		array('data' => 'комментарий', 'field' => 'mail'),
		array('data' => 'уч. запись', 'field' => 'user_status'),
		array('data' => 'материал', 'field' => 'comment_status'),
		array('data' => 'спам', 'field' => 'comment_status'),
		array('data' => '<input type="checkbox" name="quis_all_check">'),
	);
	
	$query .= tablesort_sql($header);
	$result = pager_query($query, 25, 0, NULL);
	$c = 0;
	while($row = db_fetch_object($result)) {
		$rows[$c] = array(
			array('data' => format_date($row->log_created,'custom','d M Y, H:i')),	
			array('data' => ($row->name) ? l($row->name, 'user/'.$row->uid). ' '.$row->log_uid : '...'),
			array('data' => ($row->mail) ? $row->mail : '...'),
			array('data' => ($row->comment_cid) ? l($row->subject,'node/'.$row->comment_nid) : '...'),
		);
		
		if($row->name) {
			$rows[$c][]['data'] =  ($row->user_status) ? '<span class="quis_green">активна</span>' :'<span class="quis_orange">блокирована</span>';
		}
		else {
			$rows[$c][]['data'] =  '<span class="quis_gray">удалена</span>';
		}
		if($row->comment_cid) {
			$rows[$c][]['data'] =  (!$row->comment_status) ? '<span class="quis_green">Активен</span>' :'<span class="quis_orange">Неактивен</span>';
		}
		else {
			$rows[$c][]['data'] =  '<span class="quis_gray">Удален</span>';
		}
		if($row->spam != "") {
			$rows[$c][]['data'] =  '<a href="#openModal-'.$row->log_cid.'" class="openModal" path="'.$GLOBALS['base_url'].'" nid="'.$row->log_cid.'" type="comment">Смотреть</a>'.quis_textmodal($row->log_cid);
		}
		else {
			$rows[$c][]['data'] =  '..';
		}
		$rows[$c][]['data']='<input type="checkbox" name="for_delete['.$row->id.']" value="1">';
		$c++;
	}
	
	$output .= '<form action="'.$base_url.'/admin/settings/quis/log_comments"  accept-charset="UTF-8" method="post" id="quis-admin-settings-comments">';
	$output .= theme('pager', NULL, 25, 0);
	$output .= theme('table', $header, $rows);
	$output .= theme('pager', NULL, 25, 0);
	$output .= '<div class="quis-button"><input type="submit" value="Удалить отмеченные" onClick="return confirm(\'Уверены?\')"></div>';
	$output .= '</form>';
	return $output;
}*/

function quis_goodusers() {
	drupal_add_css(drupal_get_path('module','quis').'/quis.css');
	$goodusers =  variable_get('quis_goodusers','1');
	$goodusers_array = explode(",",$goodusers);
	$str = '';
	$users = array();
	if(is_array($goodusers_array)) {
		foreach ($goodusers_array AS $uid) {
			if($uid) {
				$user = user_load($uid);
				$users[''.$user->name.''] = 'id '.$user->uid.': '.l($user->name,'user/'.$user->uid);
			}
		}
	}
	$form['quis_goodusers']  = array(
		'#type' => 'textarea',
		'#title' => 'Идентификаторы',
		'#description' => 'Введите Id пользователей чередуя запятой (без пробелов)',
		'#default_value' => $goodusers,
	);
	$form['quis_goodusers_list'] = array(
		'#access'=> intval(sizeof($users) ),
		'#type' => 'item',
		'#value' => '<div class="quis-columns-text quis-googusers-area">'.implode("<br />",$users).'</div>',
	);
	return system_settings_form($form);
}

function quis_delete_materials() {
	$spamer = variable_get('quis_spamer_id', null);
	$spamer2 = intval(variable_get('quis_spamer_id2', null));
	$form['quis_spamer_id'] = array(
		'#type' => 'textfield',
		'#title' => 'Id пользователя, начиная с',
		'#attributes' => array('style' => 'width:200px'),
		'#description' => 'Все материалы этого пользователя будут удалены',
		'#default_value' => $spamer,
	);
	$form['quis_spamer_id2'] = array(
		'#type' => 'textfield',
		'#title' => 'кончая пользователем...',
		'#attributes' => array('style' => 'width:200px'),
		'#description' => 'Если есть данные, будет учитываться интервал id спамеров для поиска материалов',
		'#default_value' => $spamer2,
	);
	$form['quis_spamer_disabled'] = array(
		'#type' => 'checkbox',
		'#title' => 'Одновременно блокировать спамера',
//		'#suffix' => 'Статус = '.db_result(db_query("SELECT status FROM {users} WHERE uid = %d", $spamer)),
		'#suffix' => 'Статус = '.db_select("users")->fields(array("status"))->condition("uid", $spamer)->execute()->fetchField(),
	);
	$form['quis_count_nodes'] = array(
		'#type' => 'textfield',
		'#title' => 'Кол-во материалов',
		'#attributes' => array('style' => 'width:150px'),
		'#default_value' => variable_get('quis_count_nodes', 0),
		'#description' =>'Сколько материала удалить? 0 - весь материал',
	);
	$form['quis_count_packed_materials'] = array(
		'#type' => 'textfield',
		'#title' => 'Пакетная обработка',
		'#attributes' => array('style' => 'width:150px'),
		'#default_value' => variable_get('quis_count_packed_materials', 1),
	);
	$form['quis_delete_start'] = array(
		'#type' => 'checkbox',
		'#title' => 'Запуск удаления',
		'#description' => 'После нажатия на кнопку "Сохранить конфигурацию" начнется процесс удаления.',
	);
	if($spamer && !$spamer2) {
		//$count_nodes = db_result(db_query("SELECT COUNT(*) FROM {node} WHERE uid IN(%s)", $spamer));
		$count_nodes = db_select("node")->countQuery()->condition("uid", $spamer,"IN")->execute()->fetchField();
	}
	elseif ($spamer && $spamer2) {
		//$count_nodes = db_result(db_query("SELECT COUNT(*) FROM {node} WHERE uid >= %d && uid <= %d", $spamer, $spamer2));
		$count_nodes = db_select("node")->countQuery()->add_where_expression(0, "uid >= $spamer && uid <= $spamer2")->execute()->fetchField();
	}
	$form['quis_show_info']= array(
		'#type' => 'item',
		'#access' => $spamer > 0,
		'#value' => $count_nodes,
		'#title' => 'Количество материалов выбранно(го)(ых) пользовател(я)(ей)',
	);
	
	$form['#submit'][] = 'quis_delete_materials_start';
	return system_settings_form($form);
}

function quis_delete_users() {
	drupal_add_css(drupal_get_path('module','quis').'/quis.css');
	drupal_add_js(drupal_get_path('module','quis').'/quis.js');
	$oldlogin = variable_get('quis_activate_profile_oldlogin', 0);
	$form['quis_block1'] = array(
		'#type' => 'fieldset',
		'#title' => 'Активность на сайте',
		'#description' => 'Включить удаление пользователей у которых последнее посещение сайта было за указанных год.',
		'#attributes' => array('class' => (($oldlogin) ? 'users-delete-selected-actions' : '')),
	);
	
	$without_materials = variable_get('quis_users_without_materials', 1);
	if($without_materials) {
		$more_sql_start ='LEFT JOIN {node} ON node.uid = users.uid';
		$more_sql_end ='&& node.nid IS NULL';
	}
	$min_year = db_query("SELECT MIN(DATE_FORMAT(FROM_UNIXTIME(users.login), '%Y')) FROM {users} ".$more_sql_start." WHERE users.login != 0 && users.uid NOT IN(0,1) && users.status = 1 ".$more_sql_end)->execute()->fetchField();
	$oldlogin = variable_get('quis_activate_profile_oldlogin', 0);
	$form['quis_block1']['quis_activate_profile_oldlogin'] = array(
		'#type' => 'checkbox',
		'#title' => 'Выбрать набор правил этого блока',
		'#description' => 'Процесс удаления будет подчиняться правилам этого блока (Активность на сайте)',
		'#default_value' => $oldlogin,
		
	);
	$form['quis_block1']['quis_users_without_materials'] = array(
		'#type' => 'checkbox',
		'#title' => 'Отсутствие материалов',
		'#description' => '...а так же, не имеют ни одного материала',
		'#default_value' => $without_materials,
	);
	
	if($min_year > 1970)  {
		for($i = $min_year; $i < date('Y') + 1; $i++) {
			$query = "SELECT COUNT(*) FROM {users} ".$more_sql_start."
			WHERE DATE_FORMAT(FROM_UNIXTIME(users.login), '%Y') = '%s' && users.uid NOT IN(0,1) && users.status = 1 ".$more_sql_end;
			//dpm($query);
			$every_year_count[$i] = 'В '.$i.'г. последний раз посещали сайт: <strong>'.db_result(db_query($query, $i)).'</strong> человек.';
		}
	}
	$form['quis_block1']['quis_touch_year'] = array(
		'#type' => 'radios',
		'#options' => $every_year_count,
		'#title' => 'Выбрать год',
		'#description' => 'Все пользователи, последний раз посещавшие сайт за указанный год',
		'#default_value' => variable_get('quis_touch_year', array(0 => $min_year)),
		'#access' => $min_year > 1970,
	);
	
	$old_created = variable_get('quis_nologin_users_old_created', array('year' => $min_year,'month' => 1,'day' => 1));
	$nologin = variable_get('quis_activate_profile_nologin', 0);
	$count_users_nologin = db_result(db_query("SELECT COUNT(*) FROM {users} WHERE created < UNIX_TIMESTAMP('%s') && login = 0 && uid NOT IN(0,1) && status = 1 ", $old_created['year'].'-'.$old_created['month'].'-'.$old_created['day']));
	$form['quis_block2'] = array(
		'#title' => '`Пустые` аккаунты',
		'#description' => 'Удаление пользователей, которые ни разу не заходили на сайт.',
		'#type' => 'fieldset',
		'#attributes' => array('class' => (($nologin) ? 'users-delete-selected-actions' : '')),
	);
	
	$form['quis_block2']['quis_activate_profile_nologin'] = array(
		'#type' => 'checkbox',
		'#title' => 'Выбрать набор правил этого блока',
		'#description' => 'Процесс удаления будет подчиняться правилам этого блока (`Пустые` аккаунты)',
		'#default_value' => $nologin,
	);
	$form['quis_block2']['quis_nologin_users_old_created'] = array(
		'#title' => 'Выбрать не позднее',
		'#description' => 'Будут удалены пользователи, зарегистрировавшиеся не позднее выше указанной даты. То есть, аккаунты созданные после указанной даты не будут учтены.',
		'#type' => 'date',
		'#default_value' => variable_get('quis_nologin_users_old_created', array()),
		'#suffix' => 'Найдено: '.intval($count_users_nologin),
	);
	#DISABLED
	$disabled = variable_get('quis_activate_profile_disabled',0);
	$disabled_count = db_result(db_query("SELECT COUNT(*) FROM {users} WHERE status = 0 && uid NOT IN(0,1)"));
	$form['quis_block3']=array(
		'#title' => 'Заблокированные пользователи',
		'#type' => 'fieldset',
		'#description' => 'Удаление заблокированных пользователей',
		'#attributes' => array('class' => (($disabled) ? 'users-delete-selected-actions' : '')),
	);
	$form['quis_block3']['quis_activate_profile_disabled'] = array(
		'#type' => 'checkbox',
		'#title' => 'Выбрать набор правил этого блока',
		'#description' => 'Процесс удаления будет подчиняться правилам этого блока (Заблокированные пользователи)',
		'#default_value' => $disabled,
		'#suffix' => 'Найдено: '.intval($disabled_count),
	);
	$form['quis_count_users'] = array(
		'#title' => 'Сколько удалить пользователей?',
		'#attributes' => array('style' => 'width:150px'),
		'#description' => '0 - все пользователи, соответствующие выбранным условиям.',
		'#type' => 'textfield',
		'#default_value' => variable_get('quis_count_users',0),
	);
	$form['quis_count_packed_users'] = array(
		'#title' => 'Попакетная обработка',
		'#type' => 'textfield',
		'#attributes' => array('style'=>'width:150px'),
		'#default_value' => variable_get('quis_count_packed_users', 1),
		'#access' => true,
	);
	$form['quis_delete_start'] = array(
		'#type' => 'checkbox',
		'#title' => 'Запуск удаления',
		'#description' => 'После нажатия на кнопку "Сохранить конфигурацию" начнется процесс удаления.',
	);
	$form['#submit'][] = 'quis_delete_users_start';
	
	return system_settings_form($form);
}
/**
 * вызов инициализации удаления материалов
 */
function quis_delete_users_start(&$form, &$form_state) {
	if($form_state['values']['quis_delete_start'] == 1) {
		$count_users = $form_state['values']['quis_count_users'];
		$count_packed = $form_state['values']['quis_count_packed_users'];
		//$count_packed = 1;
		
		$limit_date = $form_state['values']['quis_nologin_users_old_created'];
		if($form_state['values']['quis_activate_profile_oldlogin'] == 1) {
			if($form_state['values']['quis_users_without_materials'] == 1) {
				$more_sql_start ='LEFT JOIN {node} ON node.uid = users.uid';
				$more_sql_end ='&& node.nid IS NULL';
			}
			$postquery = "FROM {users} ".$more_sql_start." WHERE DATE_FORMAT(FROM_UNIXTIME(users.login), '%Y') = '%s' && users.uid NOT IN(0,1) && users.status = 1 ".$more_sql_end;
			$argument = $form_state['values']['quis_touch_year'];
			$sql_count = db_result(db_query("SELECT COUNT(*) ".$postquery, $argument));
		}
		elseif($form_state['values']['quis_activate_profile_nologin'] == 1) {
			$postquery = "FROM {users} WHERE login = 0 && uid NOT IN(0,1) && status = 1 && created < UNIX_TIMESTAMP('%s')";
			$argument = $limit_date['year']."-".$limit_date['month']."-".$limit_date['day'];
			$sql_count = db_result(db_query("SELECT COUNT(*) ".$postquery, $argument));
		}
		elseif ($form_state['values']['quis_activate_profile_disabled'] == 1) {
			$postquery = "FROM {users} WHERE status = 0 && uid NOT IN(0,1)";
			$argument = "";
			$sql_count = db_result(db_query("SELECT COUNT(*) ".$postquery));
		}
		$count = ($count_users <= $sql_count && $count_users != 0) ? $count_users : $sql_count;
		$count = ($count_packed > 0 && $count_packed <= $count_users) ? ceil($count/$count_packed) : $count;
  	$operations = array();
  	for($i = 0 ; $i < $count; $i++) {
  		$operations[] = array('quis_batch_process_users', array("SELECT users.uid, users.name ".str_replace("%s",$argument,$postquery), $count_packed));
  	}
  	if($count_packed > 1) {
  		$text = 'Идет пакетная обработка по '.$count_packed.' пользователей';
  	}
		$batch2 = array(
			'title' => 'Удаление пользователей',
			'init_message' => 'Начало процесса удаления...',
			'progress_message' => t('Выполнено @current из @total. '.$text),
    	'error_message' => t('Процесс удаления был нарушен.'),
			'operations' => $operations,
			'finished' => 'quis_batch_finished_users',
			'file' => drupal_get_path('module', 'quis') . '/quis.admin.inc',
		);
		batch_set($batch2);
	}
}
/**
 * вызов удаления пользователей
 * @param str $query
 * @param array $context
 */
function quis_batch_process_users($query, $count_packed, &$context) {
  $result = db_query_range($query, 0, $count_packed);
  while ($row = db_fetch_array($result)) {
  	user_delete(NULL, $row['uid']);
    $context['results'][] = $row['uid'] .' : '. $row['name'];
    $context['sandbox']['progress']++;
    $context['sandbox']['current_uid'] = $row['uid'];
    $context['message'] = '<div class="quis-googusers-area">'.$row['name'].'</div>';
  }
}
/**
 * финишер батч процессса удаления пользователей
 */
function quis_batch_finished_users($success, $results, $operations) {
  if ($success) {
    $message = format_plural(count($results), 'Пользователь был удален.', '@count пользователей было удалено.');
  }
  else {
    $message = t('Завершено с ошибкой.');
  }
  drupal_set_message($message);
  // Providing data for the redirected page is done through $_SESSION.
  foreach ($results as $result) {
    $items[] = t('Удален %title.', array('%title' => $result));
  }
  $_SESSION['my_batch_results'] = $items;
}
/**
 * вызов инициализации удаления материалов
 */
function quis_delete_materials_start(&$form, &$form_state) {
	if($form_state['values']['quis_delete_start'] == 1) {
		$count_packed = $form_state['values']['quis_count_packed_materials'];
		$spamer1 = $form_state['values']['quis_spamer_id'];
		$spamer2 = intval($form_state['values']['quis_spamer_id2']);
		if(!$form_state['values']['quis_count_nodes']) {
			if($spamer1 && !$spamer2) {
				$count=db_result(db_query("SELECT COUNT(*) FROM {node} WHERE uid IN(%s)", $spamer1));
			}
			elseif($spamer1 && $spamer2) {
				$count=db_result(db_query("SELECT COUNT(*) FROM {node} WHERE uid >= %d && uid <= %d", $spamer1, $spamer2));
			}
		}
		else {
			$count = $form_state['values']['quis_count_nodes'];
		}
		$count = ($count_packed > 0 && $count_packed <= $count) ? ceil($count/$count_packed) : $count;
  	$operations = array();
  	for($i = 0 ; $i < $count; $i++) {
  		$operations[] = array('quis_batch_process_materials', array($spamer1,$spamer2));
  	}
  	if($count_packed > 1) {
  		$text = 'Идет пакетная обработка по '.$count_packed.' материалов';
  	}
		$batch = array(
			'title' => 'Удаление контента',
			'init_message' => 'Начало процесса удаления...',
			'progress_message' => t('Выполнено @current из @total. '.$text),
    	'error_message' => t('Процесс удаления был нарушен.'),
			'operations' => $operations,
			'finished' => 'quis_batch_finished_materials',
			'file' => drupal_get_path('module', 'quis') . '/quis.admin.inc',
		);
		batch_set($batch);
		
		if($form_state['values']['quis_spamer_disabled']) {
			if($spamer1 && !$spamer2) {
				db_query("UPDATE {users} SET status = 0 WHERE uid = %d", $spamer1);
			}
			elseif ($spamer1 && $spamer2) {
				db_query("UPDATE {users} SET status = 0 WHERE uid >= %d && uid <= %d", $spamer1, $spamer2);
			}
		}
	}
}
/**
 * вызов удаления нод
 *
 * @param int $spamer_id
 * @param int $limited
 * @param array $context
 */
function quis_batch_process_materials($spamer_id, $spamer_id2=false, &$context) {
  $limit = variable_get('quis_count_packed_materials', 1);
  if($spamer_id && $spamer_id2) {
  	$result = db_query_range("SELECT nid, title FROM {node} WHERE uid >= %d && uid <= %d", $spamer_id, $spamer_id2, 0, $limit);
  }
  elseif($spamer_id && !$spamer_id2) {
  	$result = db_query_range("SELECT nid, title FROM {node} WHERE uid IN(%s)", $spamer_id, 0, $limit);
  }
  while ($row = db_fetch_array($result)) {
    node_delete($row['nid']);
    $context['results'][] = $row['nid'] .' : '. $row['title'];
    $context['sandbox']['progress']++;
    $context['sandbox']['current_node'] = $row['nid'];
    $context['message'] = '<div class="quis-googusers-area">'.$row['title'].'</div>';
  }
}
/**
 * финишер батч процессса
 */
function quis_batch_finished_materials($success, $results, $operations) {
  if ($success) {
    $message = format_plural(count($results), 'Один узел был обработан.', '@count узлов было обработано.');
  }
  else {
    $message = t('Завершено с ошибкой.');
  }
  drupal_set_message($message);
  // Providing data for the redirected page is done through $_SESSION.
  foreach ($results as $result) {
    $items[] = t('Загружен узел %title.', array('%title' => $result));
  }
  $_SESSION['my_batch_results'] = $items;
}
/**
 * Строить форму правил.
 * так же, выводится аякс правилом
 * при динамическом добавлении
 * 
 * @param int $id
 * @param int $rule
 * @param str $words
 * @param array $spamer
 * @param str $material
 * @param str $message
 * @return array
 */
function quis_get_rule_block($id = false, $rule = false, $words = false, $spamer = array(), $material = false,$message = false) {
	if((bool)!is_array($id) && $id) {
		$title = 'Правило #'.++$id;
		$id--;
	}
	else {
		$title = 'Новое правило';
		$id = false;
	}
	$form['rule['.$id.']'] = array(
		'#type' => 'fieldset',
		'#title' => $title,
		'#prefix' => '<div id="new-rule-block">',
		'#suffix' => '</div>',
		'#collapsed' => (bool)$id,
		'#collapsible' => (bool)$id,
		'#attributes' => array('class' => (($rule) ? 'quis-rule-active': 'quis-rule-unactive')),
	);
	
	$form['rule['.$id.']']['quis_count_rule['.$id.']'] = array(
		'#type'  => 'hidden',
		'#value' => 1,
	);
	$form['rule['.$id.']']['quis_rule['.$id.']'] = array(
		'#type' => 'checkbox',
		'#title' => 'Активировать это правило',
		'#default_value' => $rule,
	);
	$form['rule['.$id.']']['quis_rule_delete['.$id.']'] = array(
		'#type' => 'checkbox',
		'#title' => 'Удалить это правило',
		'#access' => (bool)$id,
	);
	$form['rule['.$id.']']['quis_words['.$id.']'] = array(
		'#type' => 'textarea',
		'#title' => 'Нехорошие слова и фразы',
		'#description' => 'Введите слова чередуя запятой (регистр букв необязателен).',
		'#default_value' => mb_convert_case($words, MB_CASE_LOWER, "UTF-8"),
	);
	$form['rule['.$id.']']['quis_spamers_actions['.$id.']'] = array(
		'#type' => 'checkboxes',
		'#options' => quis_options_spamers(),
		'#title' => t('Операции с выявленным спамером'),
		'#description' => 'Автоматическое действие c выявленным спаммером',
		'#default_value' => $spamer,
	);
	$form['rule['.$id.']']['quis_rules_messages['.$message.']'] = array(
		'#type' => 'textfield',
		'#title' => 'Предупреждающее сообщение',
		'#default_value' => $message,
	);
	//dpm($spamer);
	$form['rule['.$id.']']['quis_materials_actions['.$id.']'] = array(
		'#type' => 'radios',
		'#options' => quis_options_material(),
		'#title' => t('Операции с выявленным материалом'),
		'#description' => 'Автоматическое действие c выявленным спам-материалом',
		'#default_value' => $material,
	);
	return $form;
}
/**
 * Составление настроек для манипуляций
 * с найденным спамером
 *
 * @return array
 */
function quis_options_spamers() {
	return array(
		'delete' => 'Удалить (требуется большая осторожность)',
		'unaccess' => 'Закрыть доступ (рекомендовано)', 
		'log' => 'Запись в лог (quis_log)', 
		'message' => 'Вывести предупреждающее сообщение'
	);
}
/**
 * составление настроек для манипуляций
 * с найденным спам-материаалом
 *
 * @return array
 */
function quis_options_material() {
	return array(
		'unaction'=>'Ничего не делать', 
		'unstatus' => 'Блокировать', 
		'delete' => 'Удалить'
	);
}