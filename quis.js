Drupal.behaviors.quis = function(context) {
	$(".quis-modals").html('');
	$("a.openModal").click(function(){
		var $nid = $(this).attr('nid');
		var $path = $(this).attr('path');
		var $type = $(this).attr('type');
		$("#text-modal-" + $nid).html('Выполняется загрузка...').load($path + '/show_spam/' + $type + '/' + $nid + ' .spam');
	});
	$("input[name=quis_all_check]").click(function(){
		$("input[name^='for_delete']").attr('checked', this.checked);
	});
	$("input[name^='quis_activate_profile']").click(function() {
		$("input[name^='quis_activate_profile']").attr('checked', 0);
		$(this).attr('checked', 1);
		$("div[id^='edit-quis-activate-profile']").parent().removeClass("users-delete-selected-actions");
		if(this.checked) {
			var $id = $(this).attr('id');
			$("div#" + $id + "-wrapper").parent().addClass("users-delete-selected-actions");
		}
	});
	$("#edit-quis-count-packed-users, #edit-quis-count-users").change(function(){
		var $users = $("#edit-quis-count-users");
		var $packed = $("#edit-quis-count-packed-users");
		if(parseInt($users.val()) < parseInt($packed.val()) && parseInt($users.val()) > 0) {
			alert('Количество попакетной обработки не может превышать количество удалений пользователей.');
			$packed.val($users.val());
		}
	});
	$("#edit-quis-count-packed-materials, #edit-quis-count-nodes").change(function(){
		var $users = $("#edit-quis-count-nodes");
		var $packed = $("#edit-quis-count-packed-materials");
		if(parseInt($users.val()) < parseInt($packed.val()) && parseInt($users.val()) > 0) {
			alert('Количество попакетной обработки не может превышать количество удалений пользователей.');
			$packed.val($users.val());
		}
	});
	$("#edit-add-rule").click(function(){
		if(confirm('Добавить новое правило?')) {
			$("#wrapper-new-block").hide().load("/quis/add_rule #new-rule-block").fadeIn(1000);
		}
		else {
			return false;
		}
	});
	
}

 